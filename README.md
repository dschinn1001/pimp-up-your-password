
/* this listing pimp-001.c is bound to GPL 2.0 and/or GPL 3.0 */
/* this listing pimp-001.c is written by Carl-Valentin Schmitt + cv.schmitt@gmail.com */
/* this listing works only with Linux - not with Windows - and is written for Linux only */
/* this not yet has been tested on Apple - Apple allows as password at maximum 60 to 80 chars (?) */
/* Apple and Windows are registered trademarks */
/* pimp-001.c is first version - only for random numbers */
/* pimp-002.c is second version - only for random chars (ASCII 0 to 255) */

This topic is case-sensitive, otherwise you can lock off yourself from Linux Desktop.

Please read this first to end - and watch out for usage, BEFORE you change the given listing
in c for stronger passwords - you have to change your password with embedded link to
a normal password without embedded link.

The thing is how the Linux password as Desktop User, could look with this compiled listing is following :

your-linux-login-password-then-key-space-multiplicated-with-key-space-\/home\/$user\/\.pimp\; /\*(this is pimped password)\*/

(In the listing the brackets of the matrix (=tuple) should maybe left away - but how ?)

The idea is - with embedded link of your binary, your password is multiplicated with
the output of your binary.

Your binary is compiled with this listing "pimp-001.c" - this listing is
generating random numbers for your password.
"pimp-002.c" is generating random chars for your password.

When this listing is modified - while it is in usage and embedded already - , then your
binary has some bits and bytes more than before,
and your password is then no longer accepted any more. This is the danger of being locked
off from Desktop !!! So before you modify and compile your listing from new, you should
"downgrade" your password first to a password, WITHOUT embedded link. Then edit the listing
to pimp it up more. In the end, you finished your own listing and compiled it. Then
adjust your password like in line 18.

The current Linux password, must not have more than 512 chars - otherwise the over-size
chars are cut off internally. But this pimped password should then be not bigger than your given RAM.

I tested this already - and when I am not wrong, the pimped password is accepted as password this way.

Maybe it is better when the listing has at maximum 400 bytes - currently pimp-001.c is at 1,1 KBytes.
And pimp-002.c is at 1,0 KBytes.
The listing for this usage is pretty small but highly effective, because the numbers
are random numbers. (This could be improved - not to be set as numbers - instead to be set
as random charset ciphers for all international keyboards. I plan to adjust pimp-002.c for unicode
international.)

compiler is as usually done with :

gcc -ggdb -Wall pimp-001.c -o pimp

Happy Hacking. Feel free to improve it and mail this to me.
Looking forward on to your suggestions.

dschinn1001.


